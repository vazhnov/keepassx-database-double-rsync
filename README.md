## KeePassX database double sync

This simple SHELL-script runs `rsync` twice per every of 2 remote servers.

It only updates file if it is newer (`rsync` option `-u`), so time of all hosts must be synchronized.

I use it with both [KeePassX](https://keepassx.org) and [KeePassXC](https://keepassxc.org).

### How to use

- Replace hostnames in `SRV01` and `SRV02` variables.
- Place your KeePassX(C) database in `~/sync/`.
- Run this script on every host you use (for example, at your workstation, your personal laptop and your work laptop):
  * every time you start work,
  * every time you finish your work.

### Backups history

Script keeps a history of old copies in `SRV02`, in directory `~/sync/Backup-diff-history`.

## Copyright

You can share this script under MIT license.

Original script: https://gitlab.com/vazhnov/keepassx-database-double-rsync
