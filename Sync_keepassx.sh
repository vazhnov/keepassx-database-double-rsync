#!/usr/bin/env bash

# SPDX-License-Identifier: MIT

set -o nounset
set -o pipefail
shopt -s dotglob

# Break the whole script in case of Ctrl+C, not just one command (or, in case of network issues, you have to press Ctrl+C 4 times)
trap "echo Exited because Ctrl+C pressed.; exit;" SIGINT SIGTERM

SRV1="srv01.example.com"
SRV2="srv02.example.com"

# The most important option here is `-u` — "skip files that are newer on the receiver".
OPTIONS="-auv --info=stats0 --exclude Backup-diff-history --exclude *.lock"

mkdir -p ~/sync

## First sync: download from both servers, if newer.

# shellcheck disable=SC2086
rsync $OPTIONS -e "ssh -o ClearAllForwardings=yes" "$SRV1:sync/" ~/sync
# shellcheck disable=SC2086
rsync $OPTIONS "$SRV2:sync/" ~/sync

## Second sync: upload to both servers, if newer.

# shellcheck disable=SC2086
rsync $OPTIONS -e "ssh -o ClearAllForwardings=yes" ~/sync/ $SRV1:sync
# shellcheck disable=SC2086
rsync $OPTIONS --backup --backup-dir Backup-diff-history --suffix "---$(date +%F)" ~/sync/ $SRV2:sync
echo "Script finished"
